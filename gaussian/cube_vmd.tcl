#Script to render mos automatically
#
set path "path of the file"
# if your filename is  : mo_1.cube mo_2.cube etc...
set file_root "mo_"
set file_ext ".cube"
puts $path$file_root$file_ext
set isoval 0.002
# index of first and last mo to analyze, step default : 1
set mo_init 19
set mo_final 30
set step 1
color Display Background white
display projection orthographic
color Axes Labels black

for {set i $mo_init} {$i < [expr $mo_final + 1]} {incr i $step} {
    display resetview
    mol new  $path$file_root$i$file_ext
    set mid [molinfo top]
    mol modstyle 0 $mid Isosurface $isoval 0 0 0
    mol modcolor 0 $mid ColorID 0
    #mol modmaterial 0 top Transparent
    mol addrep top
    mol modstyle 1 $mid  Isosurface -$isoval 0 0 0
    mol modcolor 1 $mid ColorID 1
    #mol modmaterial 1 $mid Transparent
    scale by 0.5
    #rotate x by 45
    render snapshot "${path}pic_mo$i.tga"
    #mol delete top
}
